<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjects'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use \App\BITM\SEIP106611\Email\Email;


    
    $emailItem = new Email();
    $email = $emailItem->show($_GET["id"]);

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email List</title>
	<link rel="stylesheet" href="../../../../Resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">
  </head>
  <body>
      <div class="create_wrapper">
          <h1>Your Email List: </h1>
          <table class="table">
            <tr class="success">
              <td>ID : <?php echo $email->id; ?></td>
            </tr>
            <tr class="info">
              <td>Name : <?php echo $email->name; ?></td>
            </tr>
            <tr class="success">
              <td>Email : <?php echo $email->email; ?></td>
            </tr>
          </table>

          <p class="text-center"><a href="index.php">Go to Email List</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>