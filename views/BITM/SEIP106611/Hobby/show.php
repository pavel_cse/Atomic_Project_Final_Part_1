<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjects'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use \App\BITM\SEIP106611\Hobby\Hobby;


    
    $hobbyItem = new Hobby();
    $hobby = $hobbyItem->show($_GET["id"]);

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hobby Selection</title>
	<link rel="stylesheet" href="../../../../Resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">
  </head>
  <body>
      <div class="create_wrapper">
          <h1>Your Hobby: </h1>
          <table class="table">
            <tr class="success">
              <td>ID : <?php echo $hobby->id; ?></td>
            </tr>
            <tr class="info">
              <td>Name : <?php echo $hobby->name; ?></td>
            </tr>
            <tr class="success">
              <td>Hobby : <?php echo $hobby->hobby; ?></td>
            </tr>
          </table>

          <p class="text-center"><a href="index.php">Go to Hobby List</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>