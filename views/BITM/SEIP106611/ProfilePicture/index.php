<?php
    include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjects'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
    
    use \App\BITM\SEIP106611\ProfilePicture\ProfilePicture;
    use \App\BITM\SEIP106611\Utility\Utility;
    
    $profile = new ProfilePicture();
    $profiles = $profile->index();
    
    
    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Profile Picture</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../../Resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		<h1 class="text-center">Profile Picture</h1>
    <div class="wrapper">
        <?php echo Utility::message(); ?>
		 <p><a class="btn btn-success btn-xs" href="create.php">Add New</a></p>
		 <div class="listArea">
								<div class="tableInfo">
								
									<div class="row">
									
										<div class=" col-xs-4 col-sm-3 col-md-2">
											<form class="ajax" action="#" method="post">
												<select class="items" name="items">
													<option value="15">10</option>
													<option value="20">20</option>
													<option value="30">30</option>
													<option value="40">40</option>
												</select>
											</form>
										</div>
										
										<div class=" col-md-2">
											<a  class="btn btn-success btn-xs" href="#">SEARCH</a>
										</div>
										
										<div class="col-xs-8 col-sm-9 col-md-8">
																							
											<div class="row">
												<div class="col-xs-7 col-sm-8 col-md-4">
																
													<p class="textRight">Download list as:</p>
												</div>
												<div class="col-xs-5 col-sm-4 col-md-4">
																	
													<a class="btn btn-success btn-xs" href="#">PDF</a>
																		
													<a class="btn btn-success btn-xs" href="#">EXCEL</a>
													<a class="btn btn-warning btn-xs" href="trashed.php" class="list-btn">Trashed List</a>
																		
												</div>
											</div>
										</div>
									</div>
									
								</div>

         
           
    <table class="table table-bordered">
	<thead>
	<tr>
	  <th  class="text_align">SL</th>
	  <th  class="text_align">#ID</th>
	  <th  class="text_align">Name &dArr;</th>
	  <th  class="text_align">Profile Picture &dArr;</th>
	  <th  class="text_align">Action</th>
	</tr>
	</thead>
	<tbody>
	 <?php
	   $slno =1;
	   foreach($profiles as $profile){
	   ?>
		<tr>
			<td><?php echo $slno;?></td>
			<td><?php echo $profile->id;?></td>
			<td><a href="show.php?id=<?php echo $profile->id;?>"><?php echo $profile->name;?></a></td>
                        <td><img style="width: 200px; height: 200px;" src="upload/<?php echo $profile->profile_pic;?>" /></td>
			<td>
          <a class="btn btn-primary btn-xs" href="show.php?id=<?php echo $profile->id;?>">View</a>  
          <a class="btn btn-info btn-xs"href="edit.php?id=<?php echo $profile->id;?>">Edit</a>  
         
              <input type="hidden" name ="id" value="<?php echo $profile->id;?>">
              <input style="color:black" type="submit" class="btn btn-danger btn-xs" value="Delete">
			  <a class="btn btn-warning btn-xs" href="trash.php?id=<?php echo $profile->id;?>" class="list-btn">Trash</a>
			<a class="btn btn-success btn-xs" href="" class="list-btn">Share With Friend</a>
          
      </td>
		</tr>
	<?php
		$slno++;
	}
	?>
        
	</tbody>
	</table>
      </div>
      <p class="text-center"><a href="../../../../index.php">Go to Homepage</a></p>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    </script>
  </body>
</html>