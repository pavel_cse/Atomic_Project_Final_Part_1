
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Profile Picture</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../../Resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">

	<style type="text/css">
		#img{
			margin-left:400px;
		}
		.pro_name{
			margin-left: 270px;
		}		
		.pic{
			margin-left: 270px;
		}
	
	</style>
  </head>
  <body>
  <?php
	include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjects'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
    use \App\BITM\SEIP106611\ProfilePicture\ProfilePicture;
    use \App\BITM\SEIP106611\Utility\Utility;
    $profileItem = new ProfilePicture();
    $profile = $profileItem->show($_GET["id"]);


 ?>
      <div class="create_wrapper">
	   <br />
	  <br />
	  <br />
          <form class="form-horizontal" role="form" action="update.php" method="POST" enctype="multipart/form-data">
               <div class="form-group">
                  <label class="control-label col-sm-3" for="field1">Name:</label>
                  <div class="col-sm-3">
				  <input class="form-control" type="hidden" name="id" value="<?php echo $profile->id;?>">	
                    <input type="text" name="name" value="<?php echo $profile->name; ?>" class="form-control" id="field1" placeholder="write here" required="required" />
                  </div>
                </div>
                  
              </div>
				<div class=" ">
                   
                      <img id="img" style="width: 200px; height: 200px;" src="upload/<?php echo $profile->profile_pic;?> "/>
                </div>
			  
			  
                <div class="form-group">
                  <label class="pro_name">Profile Picture:</label><br />
                  <div class="col-sm-3">  
							  
                   <input type="file" name="profile_pic" value="<?php echo $profile->profile_pic; ?>" class="form-control pic" id="field2">
                  
                  </div>
                </div>
                <div class="form-group">        
                  <div class="col-sm-offset-3 col-sm-9">
                    <!--<button type="submit" class="btn btn-default" name="submit">Upload</button>-->
                    <input  type="submit" class="btn btn-default"  value="Update" name="submit">
                  </div>
                </div>
              </form>
          <p class="text-center"><a href="index.php">Back</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../Resource/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>