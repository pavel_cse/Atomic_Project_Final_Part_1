<?php
namespace App\BITM\SEIP106611\Email;

use \App\BITM\SEIP106611\Utility\Utility;

class Email{
    
    public $id = "";
    public $name = "";
    public $email = "";
    
    public function __construct($data = false){
        if(is_array($data) && array_key_exists("id", $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->name = trim($data['name']);
        $this->email = trim($data['email']);
    }
    
    public function index(){
        
        $genders = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "SELECT * FROM `email` WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $genders[] = $row;
        }
        return $genders;
    }
	
	public function trashed(){
        
        $emails = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "SELECT * FROM `email` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $emails[] = $row;
        }
        return $emails;
    }
    
    
    public function store(){
       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "INSERT INTO `atomicproject`.`email` (`name`,`email`) VALUES ( '".$this->name."','".$this->email."')";
        $result = mysql_query($query);
        
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Inserted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }

    public function show($id = false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        $query = "SELECT * FROM `email` WHERE id=".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;

    }

    public function update(){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "UPDATE `atomicproject`.`email` SET `name` = '".$this->name."', `email` = '".$this->email."' WHERE `email`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Updated Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again....</div>");
        }
        
        Utility::redirect('index.php');
    }

    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `atomicproject`.`email` WHERE `email`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Deleted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }
	
	
	public function trash($id = null){
			$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
		
		$this->id = $id;
        $this->deleted_at=time();
		
	
        $query = "UPDATE `atomicproject`.`email` SET `deleted_at` = '".$this->deleted_at."' WHERE `email`.`id` = ".$this->id;
		//utility::dd($query);
		$result=mysql_query($query);
		

		if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Trashed Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }
	
	
	public function recover($id = null){
			$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
		
		$this->id = $id;
	
        $query = "UPDATE `atomicproject`.`email` SET `deleted_at` = NULL WHERE `email`.`id` = ".$this->id;
		
		$result=mysql_query($query);
            
        if($result){
            Utility::message("<div class=\"message_success\">Data Has recover Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. can not recover Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }

}
